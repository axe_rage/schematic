`timescale 1ns / 1ps

module test_core;

	// Inputs
	reg CLK;
	reg [7:0] data;
	reg [2:0] addr;
	reg wr;
	reg rd;

	// Outputs
	wire [7:0] LCD_D;
	wire LCD_E;
	wire LCD_RW;
	wire LCD_RS;

	// Instantiate the Unit Under Test (UUT)
	new_control uut (
		.CLK(CLK), 
		.data(data), 
		.addr(addr), 
		.wr(wr), 
		.rd(rd), 
		.LCD_D(LCD_D), 
		.LCD_E(LCD_E), 
		.LCD_RW(LCD_RW), 
		.LCD_RS(LCD_RS)
	);

	initial begin
		// Initialize Inputs
		CLK = 0;
		data = 0;
		addr = 0;
		wr = 0;
		rd = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		data = 8'b01000100;
		wr = 1;
		
		#3000;
		wr = 0;
		
		#300;
		data = 13;
		wr = 1;

	end
	
	always #20 CLK = ~CLK;
      
endmodule

