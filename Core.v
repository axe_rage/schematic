`timescale 1ns / 1ps

module new_control(
	input CLK,
	input [7:0] data,
	input [2:0] addr, //not used
	input wr, rd, //rd not used
	
	//LCD
	output [7:0] LCD_D,
	output LCD_E,
	output LCD_RW,
	output LCD_RS
	);
	
	reg LCD_WriteTrigger;		//Tells LCD writer module to run when 1.  When high, keep it high until the writer's Done pin goes high or the write will abort.
	reg LCD_SetAddress;			//Tells LCD writer module to set the address in LCD_Address before writing a character when 1.
	reg LCD_ClearDisplay;		//Tells LCD writer module to clear the display first before writing a character when 1.
	reg [7:0] LCD_Byte;			//The CGROM address for the character you wish to write to the display (the character code). Simply the Data Byte
	reg [6:0] LCD_Address;		//The DDRAM address you wish to write the character to (screen location).
	
	reg [1:0] state, state_next;
	reg [15:0] counter_cycles;	//Used for delay loops.
	
	localparam [1:0]  
		WAIT = 2'b00, 
		WRITE = 2'b01,
		INIT = 2'b10,
		RESERVE = 2'b11;
	
	initial begin
		state = INIT;
		state_next = INIT;
		counter_cycles = 0;
		
		LCD_WriteTrigger = 0;
		LCD_SetAddress = 1; //0 = Use LCD's auto-incrementer for character address.  
		//1 = Set address to LCD_Address value.
		LCD_ClearDisplay = 1; //0 = Do not clear display first. 1 = Clear display first.
		LCD_Byte = 0; //Character code (CGROM address) for the character we want on screen.
		LCD_Address = 7'b0001000; //Display location (DDRAM address) for the character we want on screen.
	end
	
	wire [7:0] buf_Init_LCD_D;
	wire buf_Init_LCD_E;
	wire buf_Init_LCD_RW;
	wire buf_Init_LCD_RS;
	
	wire [7:0] buf_WTD_LCD_D;
	wire buf_WTD_LCD_E;
	wire buf_WTD_LCD_RW;
	wire buf_WTD_LCD_RS;
	
	wire buf_LCD_Ready;
	wire buf_LCD_WTD_Done;
	
	LCD_Init init(
		.CLK(CLK),
		.CursorBar(1),
		.CursorBlink(1),
		.LCD_D(buf_Init_LCD_D),
		.LCD_RW(buf_Init_LCD_RW),
		.LCD_RS(buf_Init_LCD_RS),
		.LCD_E(buf_Init_LCD_E),
		.Ready(buf_LCD_Ready)
	);
	
	LCD_Writer writer(
		.CLK(CLK),
		.Trigger(LCD_WriteTrigger),
		.SetAddress(LCD_SetAddress),
		.ClearDisplay(LCD_ClearDisplay),
		.Data(LCD_Byte),
		.Address(LCD_Address),
		.LCD_D(buf_WTD_LCD_D),
		.LCD_RW(buf_WTD_LCD_RW),
		.LCD_RS(buf_WTD_LCD_RS),
		.LCD_E(buf_WTD_LCD_E),
		.Done(buf_LCD_WTD_Done)
	);
	
	always @(posedge CLK) begin
		state = state_next;
		case (state)
			INIT: begin
				//Wait for LCD Init to finish
				if (buf_LCD_Ready) begin
						if (wr)
							state_next <= WRITE;
						else
							state_next <= WAIT;
				end
			end
			
			WRITE: begin
				//Write charachter
				if(buf_LCD_WTD_Done) begin
					if(counter_cycles == 1) begin
						counter_cycles <= 0;	//Reset counter for next time
						state_next <= WAIT;						
					end else begin
						LCD_WriteTrigger <= 0; //Delay one cycle to let the writer module reset itself.
						counter_cycles <= counter_cycles + 1;
					end
				end else begin
					LCD_WriteTrigger <= 1;		//Tell writer module to start writing.
					LCD_SetAddress <= 0;
					LCD_Address <= 0;
					LCD_ClearDisplay <= 0;		//Do not clear display.
					LCD_Byte <= data;	
				end
			end
			
			WAIT: begin
						if (wr) 
							state_next <= WRITE;
					end
		endcase
	end
		 
	assign LCD_D = buf_LCD_Ready ? buf_WTD_LCD_D : buf_Init_LCD_D;
	assign LCD_E = buf_LCD_Ready ? buf_WTD_LCD_E : buf_Init_LCD_E;
	assign LCD_RW = 0; //We never read from LCD
	assign LCD_RS = buf_LCD_Ready ? buf_WTD_LCD_RS : buf_Init_LCD_RS;

endmodule
