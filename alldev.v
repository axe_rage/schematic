`timescale 1ns / 1ps

module alldev(
	input wire clk,
   input wire reset,
	input wire rx,
	input wire btn,
	
	output tx,
	output [7:0] LCD_D,
	output LCD_RW,
	output LCD_RS,
	output LCD_E,
	output [7:0] led
);
	 
localparam
    ADDR_WIDTH = 1,
    DATA_WIDTH = 8;
    
    wire [ADDR_WIDTH-1:0] AWADDR, ARADDR; 
    wire [2:0] AWPROT, ARPROT; 
    wire AWVALID, WREADY; 
    wire [DATA_WIDTH-1:0] WDATA, RDATA; 
    wire [DATA_WIDTH/8 - 1 :0] WSTRB; 
    wire [1:0] BRESP, RRESP;
    wire BVALID, BREADY, ARREADY, ARVALID; 
    wire AWREADY, WVALID, RVALID, RREADY;     
    
    master #(
        .ADDR_WIDTH(ADDR_WIDTH), 
        .DATA_WIDTH(DATA_WIDTH))
    master_dev
    (
        .ACLK(clk), 
        .ARESETn(reset), 
        .AWREADY(AWREADY), 
        .AWADDR(AWADDR), 
        .AWPROT(AWPROT), 
        .AWVALID(AWVALID), 
        .WREADY(WREADY), 
        .WDATA(WDATA), 
        .WSTRB(WSTRB), 
        .WVALID(WVALID), 
        .BRESP(BRESP), 
        .BVALID(BVALID), 
        .BREADY(BREADY),
		  .rx(rx),
		  .btn(btn),
		  .tx(tx),
		  .led(led)
    );
    
    slave slave_dev (
        .ACLK(clk), 
        .ARESETn(reset), 
        .AWREADY(AWREADY), 
        .AWADDR(AWADDR), 
        .AWPROT(AWPROT), 
        .AWVALID(AWVALID), 
        .WREADY(WREADY), 
        .WDATA(WDATA), 
        .WSTRB(WSTRB), 
        .WVALID(WVALID), 
        .BRESP(BRESP), 
        .BVALID(BVALID), 
        .BREADY(BREADY),
		  .LCD_D(LCD_D),
		  .LCD_RW(LCD_RW),
		  .LCD_RS(LCD_RS),
		  .LCD_E(LCD_E)
    );


endmodule
