`timescale 1ns/1ps
module fifo
#(
parameter B=8,//numberofbitsinaword
W=4//numberofaddressbits
)
(
input wire clk,reset,
input wire rd,wr,
input wire [B-1:0] w_data,
output wire empty,full,
output wire [B-1:0] r_data
);
//signaldeclaration
reg[B-1:0] array_reg [2**W-1:0];//registerarray
reg[W-1:0] w_ptr_reg,w_ptr_next,w_ptr_succ;
reg[W-1:0] r_ptr_reg,r_ptr_next,r_ptr_succ;
reg full_reg,empty_reg,full_next,empty_next;
wire wr_en;

//body
//registerfilewriteoperation
always@(posedge clk)
if(wr_en)
array_reg[w_ptr_reg] <= w_data;
//registerfilereadoperation
assign r_data=array_reg[r_ptr_reg];
//writeenabledonlywhenFIFOisnotfzill
assign wr_en=wr & ~full_reg;

//fifocontrollogic
//registerforreadandw'ritepointers
always@(posedge clk,posedge reset)
if(reset)
begin
w_ptr_reg<=0;
r_ptr_reg<=0;
full_reg<=1'b0;
empty_reg<=1'b1;
end
else
begin
w_ptr_reg<=w_ptr_next;
r_ptr_reg<=r_ptr_next;
full_reg<=full_next;
empty_reg<=empty_next;
end
//next_statelogicforreadandwritepointers
always@*
begin
//successivepointervalues
w_ptr_succ=w_ptr_reg+1;
r_ptr_succ=r_ptr_reg+1;
//default:keepoldvalues
w_ptr_next=w_ptr_reg;
r_ptr_next=r_ptr_reg;
full_next=full_reg;
empty_next=empty_reg;
case({wr,rd})
2'b00: begin
end
2'b01://read
if(~empty_reg)//notempty
begin
r_ptr_next=r_ptr_succ;
full_next=1'b0;
if(r_ptr_succ==w_ptr_reg)
empty_next=1'b1;
end
2'b10://write
if(~full_reg)//notfull
begin
w_ptr_next=w_ptr_succ;
empty_next=1'b0;
if(w_ptr_succ==r_ptr_reg)
full_next=1'b1;
end
2'b11://writeandread
begin
w_ptr_next=w_ptr_succ;
r_ptr_next=r_ptr_succ;
end
endcase
end

//output
assign full=full_reg;
assign empty=empty_reg;
endmodule

