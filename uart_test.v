`timescale 1ns/1ps

module uart_test

(
input wire clk, reset,
input wire rx,
input wire btn,
output wire tx,
output wire [3:0] sw,
output wire [7:0] led, res
);
wire tx_full,rx_empty,btn_tick, rx_full;
wire [7:0] rec_data,rec_data1;
uart uart_unit
(.clk(clk),.reset(reset),.rd_uart(btn_tick),.wr_uart(btn_tick),.rx(rx),.w_data(rec_data1),.tx_full(tx_full),.rx_empty(rx_empty),.r_data(rec_data),.tx(tx), .rx_full(rx_full));
//instantiatedebouncecircuit
 debounce btn_db_unit 
(.clk(clk),.reset(reset),.sw(btn),.db_level(),.db_tick(btn_tick));
//incrernenteddataloopsback
assign rec_data1=rec_data;//zdesya +1 bilya blya
//LEDdisplay
wire sseg={1'b1,~tx_full,2'b11,~rx_empty,3'b111};
assign res = rec_data;
assign led={rx,tx,btn,btn_tick,reset,tx_full,rx_empty,rx_full};//(reset)? 8'b10101010 : 8'b11100010;

endmodule
