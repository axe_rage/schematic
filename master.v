`timescale 1ns / 1ps

module master
#(
    parameter
        ADDR_WIDTH = 1,
        DATA_WIDTH = 8,
		  RESP_OKAY = 00,
		  RESP_SLVERR = 10
)
(

// Global signals
    input wire ACLK,
    input wire ARESETn,
	
// Write address channel signals
    input wire AWREADY,

    output reg [ADDR_WIDTH-1:0] AWADDR,
    output reg [2:0] AWPROT,
    output reg AWVALID,
    
// Write data channel
    input wire WREADY,
    
    output reg [DATA_WIDTH-1:0] WDATA,
    output reg [DATA_WIDTH/8 - 1 :0] WSTRB,
    output reg WVALID,
    
// Write response channel signals
    input wire [1:0] BRESP,
    input wire BVALID,
    
    output reg BREADY,
    
//UART signals
	input wire rx,
	input wire btn,
	
	output tx,
	output [7:0] led
);

    wire reset = ARESETn;
	 wire loc_btn = btn;

//UART signals 
	 wire [7:0] UART_data;
	
	 uart_test devUART
	 (
		.clk(ACLK), 
		.reset(reset), //not used
		.rx(rx),
		.btn(btn),
		.tx(tx),
		.sw(), //not used
		.led(led),
		.res(UART_data)
	 );

    localparam MEM_LEN = 1;

    reg [DATA_WIDTH-1:0] mem;
    
// Write FSM

    reg [ADDR_WIDTH-1:0] AWADDR_next;
    reg [2:0] AWPROT_next;
    reg [DATA_WIDTH-1:0] WDATA_next;
    reg [DATA_WIDTH/8 - 1 :0] WSTRB_next;
    reg WVALID_next, BREADY_next, AWVALID_next;
    
    reg [1:0] wstate, wstate_next;
    
    localparam
        RESET = 0,
        VALID = 1,
        READY = 2,
        RESP = 3;

    always@(posedge reset, posedge ACLK)
        if(reset)
            begin
                AWVALID <= 0;
                WVALID <= 0;
                AWADDR <= 0;
                wstate <= RESET;
            end
        else
            begin
                AWADDR  <= AWADDR_next;
                AWPROT  <= AWPROT_next;
                AWVALID <= AWVALID_next;
                
                WDATA   <= WDATA_next;
                WSTRB   <= WSTRB_next;
                WVALID  <= WVALID_next;
    
                BREADY  <= BREADY_next;
                
                wstate  <= wstate_next;
            end

    always@(wstate, BVALID, AWREADY, WREADY, BRESP, loc_btn)
        begin
            AWADDR_next = AWADDR;
            AWPROT_next = AWPROT;
            AWVALID_next = AWVALID;
                
            WDATA_next = WDATA;
            WSTRB_next = WSTRB;
            WVALID_next = WVALID;
    
            BREADY_next = BREADY;
            
            wstate_next = wstate;
            
            case(wstate)
                RESET: 
					 if (loc_btn)
						 begin
							wstate_next = VALID;
							mem = UART_data;
						 end
                VALID: begin
                        AWVALID_next = 1;
                        AWADDR_next = (AWADDR == MEM_LEN)? 0: AWADDR + 1;
                        AWPROT_next = 3'b010;                  
                        
                        WVALID_next = 1;
                        WDATA_next = mem;
                        
                        // all data accesses use the  full width of the data bus
                        WSTRB_next = {(DATA_WIDTH/8){1'b1}}; 
                        
                        BREADY_next = 1;
                        
                        wstate_next = READY;
                    end
                READY: if(AWREADY & WREADY)
                    begin
                        WVALID_next = 0;
                        AWVALID_next = 0;
                        
                        wstate_next = RESP;
                    end             
                RESP: if(BVALID)
                    begin
                        // the response
                        BREADY_next = 0;
                        wstate_next = RESET;
                    end
            endcase
        end
endmodule
