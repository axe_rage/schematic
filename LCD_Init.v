`timescale 1ns / 1ps

module LCD_Init(
		 input CLK,
		 input CursorBlink,
		 input CursorBar,
		 output [7:0] LCD_D,
		 output LCD_RS,
		 output LCD_RW,
		 output LCD_E,
		 output Ready
    );

	reg [31:0] counter_cycles;
	reg [7:0] caseindex;

	reg [7:0] ib_DB;
	reg ib_RS;
	reg ib_E;
	
	reg ib_Ready;

	initial begin
		counter_cycles = 0;
		caseindex = 0;
		
		ib_DB = 8'b00000000;
		ib_RS = 0;
		ib_E = 0;
		
		ib_Ready = 0;
	end

	always @(posedge CLK) begin
		case(caseindex)
			0: begin
				//Delay

				if (counter_cycles == 10) begin //for LCD: 25000000, for simulation: 10
					counter_cycles <= 0;
					caseindex <= 1;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			//*** *** *** FUNCTION SET *** *** ***

			1: begin
				//Set Data, RS, and E - Then Wait for Data Setup

				ib_DB <= 8'b00111000;
				ib_RS <= 0;
				ib_E <= 0;
				caseindex <= 2;
			end

			2: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 10, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 3;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			3: begin
				//Raise E - Then Wait for Recognition

				ib_E <= 1;
				caseindex <= 4;
			end

			4: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 25, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 5;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			5: begin
				//Drop E - Then Wait for Command to Complete

				ib_E <= 0;
				caseindex <= 6;
			end

			6: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 2500, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 7;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			7: begin
				//Delay

				if (counter_cycles == 2) begin //for LCD: 300000, for simulation: 2
					counter_cycles <= 0;
					caseindex <= 8;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			//*** *** *** ENTRY MODE SET *** *** ***

			8: begin
				//Set Data, RS, and E - Then Wait for Data Setup

				ib_DB <= 8'b00000110;
				ib_RS <= 0;
				ib_E <= 0;
				caseindex <= 9;
			end

			9: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 10, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 10;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			10: begin
				//Raise E - Then Wait for Recognition

				ib_E <= 1;
				caseindex <= 11;
			end

			11: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 25, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 12;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			12: begin
				//Drop E - Then Wait for Command to Complete

				ib_E <= 0;
				caseindex <= 13;
			end

			13: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 2500, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 14;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			//*** *** *** DISPLAY ON *** *** ***

			14: begin
				//Set Data, RS, and E - Then Wait for Data Setup

				ib_DB <= {6'b000011, CursorBar, CursorBlink};
				ib_RS <= 0;
				ib_E <= 0;
				caseindex <= 15;
			end

			15: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 10, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 16;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			16: begin
				//Raise E - Then Wait for Recognition

				ib_E <= 1;
				caseindex <= 17;
			end

			17: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 25, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 18;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			18: begin
				//Drop E - Then Wait for Command to Complete

				ib_E <= 0;
				caseindex <= 19;
			end

			19: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 2500, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 20;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end
			
			//*** *** *** CLEAR DISPLAY *** *** ***

			20: begin
				//Set Data, RS, and E - Then Wait for Data Setup

				ib_DB <= 8'b00000001;
				ib_RS <= 0;
				ib_E <= 0;
				caseindex <= 21;
			end

			21: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 1, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 22;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			22: begin
				//Raise E - Then Wait for Recognition

				ib_E <= 1;
				caseindex <= 23;
			end

			23: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 25, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 24;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			24: begin
				//Drop E - Then Wait for Command to Complete

				ib_E <= 0;
				caseindex <= 25;
			end

			25: begin
				//Delay

				if (counter_cycles == 2) begin //for LCD: 250000, for simulation: 2
					counter_cycles <= 0;
					caseindex <= 26;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end
			
			//*** *** *** SET DDRAM Address *** *** ***

			26: begin
				//Set Data, RS, and E - Then Wait for Data Setup

				ib_DB <= 8'b10000000;
				ib_RS <= 0;
				ib_E <= 0;
				caseindex <= 27;
			end

			27: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 10, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 28;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			28: begin
				//Raise E - Then Wait for Recognition

				ib_E <= 1;
				caseindex <= 29;
			end

			29: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 25, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 30;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			30: begin
				//Drop E - Then Wait for Command to Complete

				ib_E <= 0;
				caseindex <= 31;
			end

			31: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 2500, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 32;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end
			
			32: begin
				ib_Ready <= 1;
			end
		endcase
	end

	assign LCD_D = ib_Ready ? 8'b00000000 : ib_DB;
	assign LCD_RS = ib_Ready ? 1'b0 : ib_RS;
	assign LCD_RW = 0;
	assign LCD_E = ib_Ready ? 1'b0 : ib_E;
	
	assign Ready = ib_Ready;

endmodule
