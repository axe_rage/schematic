`timescale 1ns / 1ps

//	Usage:	When idle, Trigger should be 0.  All other inputs can be anything.
//			Set Address, Data, SetAddress, and ClearDisplay.
//			Wait until Done raises to 1.
//			Set Trigger to 0, then wait one cycle.
//			Repeat as desired.

module LCD_Writer(
		 input CLK,				//Must be 50 MHz or slower
		 input Trigger,			//Must be set to 1 for this module to complete operation.
		 input SetAddress,		//Optionally, sets an explicit address before writing this byte.
		 input ClearDisplay,	//Optionally, clears the entire display before writing this byte.
		 input [7:0] Data,		//The DDRAM byte to write to the current address.
		 input [6:0] Address,	//The DDRAM address to write to if SetAddress is 1.
		 output [7:0] LCD_D,	
		 output LCD_RS,
		 output LCD_RW,
		 output LCD_E,
		 output Done
    );

	reg [31:0] counter_cycles;
	reg [7:0] caseindex;

	reg [7:0] ib_DB;
	reg ib_RS;
	reg ib_E;
	
	reg ib_Done;
	
	reg last_Trigger;

	initial begin
		counter_cycles = 0;
		caseindex <= 0;
		
		ib_DB = 0;
		ib_RS = 0;
		ib_E = 0;
		
		ib_Done = 0;
		
		last_Trigger = 0;
	end

	always @(posedge CLK) begin
		if (Trigger == 1 && last_Trigger == 0 && caseindex == 0) begin
			//Raising edge of trigger and we're currently not doing anything
			
			if (ClearDisplay) begin
				caseindex <= 1;
			end else if (SetAddress) begin
				caseindex <= 7;
			end else begin
				caseindex <= 13;
			end
		end
		
		if (!Trigger) begin
			caseindex <= 0;
			ib_Done <= 0;
		end
		
		case(caseindex)
			//*** *** *** CLEAR DISPLAY *** *** ***

			1: begin
				//Set Data, RS, and E - Then Wait for Data Setup

				ib_DB <= 8'b00000001;
				ib_RS <= 0;
				ib_E <= 0;
				caseindex <= 2;
			end

			2: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 10, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 3;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			3: begin
				//Raise E - Then Wait for Recognition

				ib_E <= 1;
				caseindex <= 4;
			end

			4: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 25, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 5;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			5: begin
				//Drop E - Then Wait for Command to Complete

				ib_E <= 0;
				caseindex <= 6;
			end

			6: begin
				//Delay

				if (counter_cycles == 2) begin //for LCD: 250000, for simulation: 2
					counter_cycles <= 0;
					
					if (SetAddress) begin
						caseindex <= 7;
					end else begin
						caseindex <= 13;
					end
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			//*** *** *** SET DDRAM Address *** *** ***

			7: begin
				//Set Data, RS, and E - Then Wait for Data Setup

				ib_DB <= {1'b1, Address};
				ib_RS <= 0;
				ib_E <= 0;
				caseindex <= 8;
			end

			8: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 10, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 9;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			9: begin
				//Raise E - Then Wait for Recognition

				ib_E <= 1;
				caseindex <= 10;
			end

			10: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 25, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 11;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			11: begin
				//Drop E - Then Wait for Command to Complete

				ib_E <= 0;
				caseindex <= 12;
			end

			12: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 2500, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 13;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			//*** *** *** WRITE DDRAM BYTE *** *** ***

			13: begin
				//Set Data, RS, and E - Then Wait for Data Setup

				ib_DB <= Data;
				ib_RS <= 1;
				ib_E <= 0;
				caseindex <= 14;
			end

			14: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 10, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 15;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			15: begin
				//Raise E - Then Wait for Recognition

				ib_E <= 1;
				caseindex <= 16;
			end

			16: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 25, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 17;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			17: begin
				//Drop E - Then Wait for Command to Complete

				ib_E <= 0;
				caseindex <= 18;
			end

			18: begin
				//Delay

				if (counter_cycles == 1) begin //for LCD: 2500, for simulation: 1
					counter_cycles <= 0;
					caseindex <= 19;
				end else begin
					counter_cycles <= counter_cycles + 1;
				end
			end

			19: begin
				if (Trigger) begin
					ib_Done <= 1;
				end
			end
		endcase
		
		last_Trigger <= Trigger;
	end

	assign LCD_D = (ib_Done || (!Trigger)) ? 8'b00000000 : ib_DB;
	assign LCD_RS = (ib_Done || (!Trigger)) ? 1'b0 : ib_RS;
	assign LCD_RW = 0;
	assign LCD_E = (ib_Done || (!Trigger)) ? 1'b0 : ib_E;
	
	assign Done = ib_Done;

endmodule
