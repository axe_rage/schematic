`timescale 1ns / 1ps

module slave
#(
    parameter
        ADDR_WIDTH = 1,
        DATA_WIDTH = 8,
		  RESP_OKAY = 00,
		  RESP_SLVERR = 10
)
(

// Global signals
    input wire ACLK,
    input wire ARESETn,
	
// Write address channel signals
    input wire [ADDR_WIDTH-1:0] AWADDR,
    input wire [2:0] AWPROT,
    input wire AWVALID,
    
    output reg AWREADY,
    
// Write data channel
    input wire [DATA_WIDTH-1:0] WDATA,
    input wire [DATA_WIDTH/8 - 1 :0] WSTRB,
    input wire WVALID,
    
    output reg WREADY,
    
// Write response channel signals
    input wire BREADY,
    
    output reg [1:0] BRESP,
    output reg BVALID,
	 
//LCD signals
	 output [7:0] LCD_D,
	 output LCD_RS,
	 output LCD_RW,
	 output LCD_E
);

// reg file
    localparam MEM_MAX_ADDR = 1;
    
    reg [DATA_WIDTH-1:0] mem;

// Write FSM
    wire reset = ARESETn;
    
    reg AWREADY_next, WREADY_next;
    reg [1:0] BRESP_next;
    reg BVALID_next;
   
    reg [1:0] wstate, wstate_next;
	 
//LCD signals
	 reg wr_next;
	 reg wr = 0;
	 reg [7:0] LCD_data;
    
	new_control devLCD
	(
		.CLK(ACLK),
		.data(LCD_data),
		.addr(), //not used
		.wr(wr), 
		.rd(), //not used
		.LCD_D(LCD_D),
		.LCD_E(LCD_E),
		.LCD_RW(LCD_RW),
		.LCD_RS(LCD_RS)
	);
	 
    localparam
        RESET = 0,
        READY = 1,
        VALID = 2,
        RESP = 3;
   
    always@(posedge reset, posedge ACLK)
        if(reset)
            begin
                BVALID <= 0;
                wstate <= RESET;
					 wr <= 0;
            end
        else
            begin
                AWREADY <= AWREADY_next;
                
                WREADY  <= WREADY_next;
               
                BRESP   <= BRESP_next;
                BVALID  <= BVALID_next;

					 wr <= wr_next;
                wstate  <= wstate_next;
            end
            
     always@*
        begin
            AWREADY_next = AWREADY;
            WREADY_next = WREADY;
            BRESP_next = BRESP;
            BVALID_next = BVALID;
            
				wr_next = wr;
            wstate_next = wstate;
            
            case(wstate)
                RESET: begin
						wstate_next = READY;
						wr_next = 0;
					 end
                READY:
                    begin
                        AWREADY_next = 1;
                        WREADY_next = 1;
                        
								wr_next = 0;
                        wstate_next = VALID;
                    end
                VALID:if(AWVALID & WVALID)
                    begin
                        if(AWADDR > MEM_MAX_ADDR)
                            BRESP_next = RESP_SLVERR;
                        else
                            begin
                                mem = WDATA; 
										  LCD_data = mem;
                                BRESP_next = RESP_OKAY;
                            end
                             
                        BVALID_next = 1;
                        AWREADY_next = 0;
                        WREADY_next = 0;
                       
							   wr_next = 1;
                        wstate_next = RESP;
                    end
                RESP: if(BREADY)
                    begin
						  		wr_next = 0;
                        BVALID_next = 0;
                        wstate_next = READY;
                    end
            endcase
        end
endmodule
