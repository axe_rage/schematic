module uart
#(
	parameter 
	DBIT = 8, // # d a t a b i t s
	SB_TICK = 16 , // # t i c k s f o r s t o p b i t s ,
// 1 6 / 2 4 / 3 2 f o r 1/1.5/2 b i t s
	DVSR = 163, // baud r a t e d i v i s o r
// DVSR = 50M/(16* baud r a t e )
	DVSR_BIT = 8, // # b i t s o f DVSR
	FIFO_W = 2 // # addr b i t s o f FIFO
	)
	(
	input wire clk,reset,
   input wire rd_uart,wr_uart,rx,
   input wire [7:0] w_data,
   output wire tx_full,rx_empty,tx, rx_full,
   output wire [7:0] r_data
);
   wire tick,rx_done_tick,tx_done_tick;
	wire tx_empty,tx_fifo_not_empty;
	wire [7:0] tx_fifo_out,rx_data_out;

	/*
	inputwireclk,reset,
	//inputwirerx,
	inputwire[7:0]din,
	inputwires_tick,tx_start,
	//outputwiretx,
	outputwire[7:0]dout
	);
	
	wirerx,tx;
	wirerx_done_tick,tx_done_tick;*/
	//wire[7:0]tx_out,rx_out;
	mod_m_counter #(.M(DVSR),.N(DVSR_BIT)) baud_gen_unit(.clk(clk),.reset(reset),.q(),.max_tick(tick));
	
	//uart_rx#(.DBIT(DBIT),.SB_TICK(SB_TICK))uart_rx_unit(.clk(clk),.reset(reset),.rx(rx),.s_tick(s_tick),.rx_done_tick(rx_done_tick),.dout(dout));
	//uart_tx#(.DBIT(DBIT),.SB_TICK(SB_TICK))uart_tx_unit(.clk(clk),.reset(reset),.tx_start(tx_start),.s_tick(s_tick),.din(din),.tx_done_tick(tx_done_tick),.tx(tx));
	uart_rx#(.DBIT(DBIT),.SB_TICK(SB_TICK))uart_rx_unit(.clk(clk),.reset(reset),.rx(rx),.s_tick(tick),.rx_done_tick(rx_done_tick),.dout(rx_data_out));
	fifo#(.B(DBIT),.W(FIFO_W))fifo_tx_unit(.clk(clk),.reset(reset),.rd(tx_done_tick),.wr(wr_uart),.w_data(w_data),.empty(tx_empty),.full(tx_full),.r_data(tx_fifo_out));
	fifo#(.B(DBIT),.W(FIFO_W))fifo_rx_unit(.clk(clk),.reset(reset),.rd(rd_uart),.wr(rx_done_tick),.w_data(rx_data_out),.empty(rx_empty),.full(rx_full),.r_data(r_data));
	uart_tx#(.DBIT(DBIT),.SB_TICK(SB_TICK))uart_tx_unit(.clk(clk),.reset(reset),.tx_start(tx_fifo_not_empty),.s_tick(tick),.din(tx_fifo_out),.tx_done_tick(tx_done_tick),.tx(tx));
	assign tx_fifo_not_empty = ~tx_empty;
	endmodule
	